import { Component} from '@angular/core';
import { DeseosService } from 'src/app/services/deseos.service';
import { ActivatedRoute } from '@angular/router';
import { ListaItem } from 'src/app/modelos/lista-item.models';
import { Lista } from 'src/app/modelos/lista.model';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage  {
  lista: Lista;
  nombreItems = "";

  constructor(private deseoServices: DeseosService, private route: ActivatedRoute ) {
      this.route.params.subscribe( data => {
        this.lista = this.deseoServices.obtenerLista(data['idLista']);
      })
  }

  agregarItems(){
      if(this.nombreItems.length === 0){
        return;
      }
      const nuevoItems = new ListaItem( this.nombreItems );
      this.lista.items.push(nuevoItems);
      this.nombreItems = "";
      this.deseoServices.agregarLocalstore();
  }

  cambioCompletado(lista: ListaItem){
    const tareasFaltantes = this.lista.items.filter(data => !data.completado).length;
    console.log({tareasFaltantes});

    if(tareasFaltantes === 0){
      this.lista.termindaEn = new Date();
      this.lista.terminada = true;

    }else{
      this.lista.termindaEn = null;
      this.lista.terminada = false;
    }

    this.deseoServices.agregarLocalstore();
    console.log(this.lista);

  }

  borrar(item: number){
    this.lista.items.splice(item, 1);
    this.deseoServices.agregarLocalstore();

  }

}
