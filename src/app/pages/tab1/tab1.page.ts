import { Component } from '@angular/core';
import { DeseosService } from '../../services/deseos.service';
import {Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Lista } from 'src/app/modelos/lista.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  lista: any[]=[];

  constructor(public deseosServices: DeseosService, private router: Router,
    private alertController: AlertController ){
     this.lista= this.deseosServices.listas;
  }

  agregarLista(id: number ){
    this.router.navigateByUrl(`/tabs/tab1/agregar/${id}`);
  }

  async presentAlert(){
    const alert = await this.alertController.create({
      header: 'Agregar tarea',
      inputs: [
        {
        name: 'titulo',
        type: "text",
        placeholder: 'ingrese tarea'
        }
      ],
      buttons: [
        {
          text:"cancelar",
          role:'cancel'
        },
        {
          text: "Agregar",
          handler: (data) => { 
            if(data.titulo.length === 0){
              return;
            }else{
              const idLista = this.deseosServices.agregarListaServices(data.titulo);
              this.agregarLista(idLista );
            }
          }
        }
      ]
    });

     alert.present();
  }

  listaSeleccionada(list: Lista){
    this.router.navigateByUrl(`/tabs/tab1/agregar/${list.id}`);
  }

}
