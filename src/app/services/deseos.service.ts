import { Injectable } from '@angular/core';
import { Lista } from '../modelos/lista.model';


@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  listas: Lista[] = [];

  constructor() {
    this.obtenerLocalstore();
   }

   agregarListaServices(titulo: string){
    const lista = new Lista(titulo);
    this.listas.push(lista);
    this.agregarLocalstore();
    return lista.id;
   }


   obtenerLista(id: string | number){
     id = Number(id);
     return this.listas.find ( data =>  data.id === id);
   }

   agregarLocalstore(){
     localStorage.setItem('data', JSON.stringify( this.listas ));
   }

   obtenerLocalstore(){
     if (localStorage.getItem('data')){
      this.listas = JSON.parse(localStorage.getItem('data'));
     }  
  }
}
